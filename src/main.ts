import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import Buefy from "buefy";
import "buefy/dist/buefy.css";

import {
  Swiper as SwiperClass,
  Pagination,
  Navigation,
  Mousewheel,
  Autoplay
} from "swiper";
import getAwesomeSwiper from "vue-awesome-swiper/dist/exporter";
// import style (>= Swiper 6.x)
import "swiper/swiper-bundle.css";

Vue.config.productionTip = false;

Vue.use(Buefy);

SwiperClass.use([Pagination, Mousewheel, Navigation, Autoplay]);
Vue.use(getAwesomeSwiper(SwiperClass));

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

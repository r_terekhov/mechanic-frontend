interface LocalStorage {
  wasStartScreentDisplayed: boolean;
}

export const wasStartScreentDisplayed = () => {
  const ifswd = localStorage.getItem("meta");
  if (!ifswd) {
    return false;
  }
  const meta: LocalStorage = JSON.parse(ifswd);
  return meta.wasStartScreentDisplayed;
};

export const setWasStartScreenDisplayed = () => {
  const ls: LocalStorage = {
    wasStartScreentDisplayed: true
  };
  localStorage.setItem("meta", JSON.stringify(ls));
};

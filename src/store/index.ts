import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    needDiagnostic: false
  },
  mutations: {
    SET_NEED_DIAGNOSTIC(state, value) {
      state.needDiagnostic = value;
    }
  },
  getters: {
    needDiagnostic(state) {
      return state.needDiagnostic;
    }
  },
  actions: {},
  modules: {}
});

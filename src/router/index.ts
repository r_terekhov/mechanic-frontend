import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

import Start from "@/views/Start.vue";
import Welcome from "@/views/Welcome.vue";

import CustomerServiceSelect from "@/views/customer/ServiceSelect.vue";
import CustomerMechanicCall from "@/views/customer/MechanicCall.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Start",
    component: Start
  },
  {
    path: "/service-select",
    name: "ServiceSelect",
    component: CustomerServiceSelect
  },
  {
    path: "/mechanic-call",
    name: "MechanicCall",
    component: CustomerMechanicCall
  },
  {
    path: "/welcome",
    name: "Welcome",
    component: Welcome
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

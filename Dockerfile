# pull official base image
FROM node:13.12.0-alpine

# set working directory
WORKDIR /app
ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install
RUN npm install @vue/cli@3.7.0 -g

# add app
COPY . ./

EXPOSE 3000
# start app
CMD ["npm", "run", "serve"]
